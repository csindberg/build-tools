﻿function Get-MsBuildExec([String] $toolsVersion) {
    if($toolsVersion -eq "15.0") {
        $result = "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MsBuild.exe"
        if(Test-Path -Path $result) {
            return $result
        }

        $result = "${env:ProgramFiles}\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MsBuild.exe"
        if(Test-Path -Path $result) {
            return $result
        }

        throw [System.Exception] "You don't have version '$toolsVersion' of msbuild tools installed"
    }
    else {
        Try {
            $regKey = "HKLM:\software\Microsoft\MSBuild\ToolsVersions\$toolsVersion"
            $regProperty = "MSBuildToolsPath"
            $msbuildExe = join-path -path (Get-ItemProperty $regKey).$regProperty -childpath "msbuild.exe"
            return $msbuildExe
        } Catch {
            throw [System.Exception] "You don't have version '$toolsVersion' of msbuild tools installed"
        }
    }
}