﻿function ScPack-VerifyPackage($folder, $packageName, $sitecoreVersion, $octopusServer, $octopusApiKey) {
    $fullPackageName = "$packageName-$sitecoreVersion"
    $fileName = "$packageName.$sitecoreVersion.zip"
    $fullFilePath = [System.IO.Path]::Combine($folder, $fileName)

    Write-Host "Checking for file '$fullFilePath'"
    $shouldDownload = $true

    if(Test-Path -PathType Leaf $fullFilePath) {
        Write-Host "File found. Checking hash."
        # Use url below to get the sha1 hash use to see if we already have the current file
        $hashRequestUrl = "$octopusServer/api/packages/packages-" + $fullPackageName + "?apiKey=$octopusApiKey"
        $hash = Invoke-WebRequest $hashRequestUrl |
            ConvertFrom-Json |
            Select -ExpandProperty Hash
        $calculatedHash = Get-FileHash -Algorithm SHA1 $fullFilePath | select -ExpandProperty Hash
        if($calculatedHash -ne $hash) {
            Write-Host "Hash mismatch. Redownloading package."
            $shouldDownload = $true
        } else {
            Write-Host "Hash matches. Package integrity verified."
            $shouldDownload = $false
        }
    }

    if($shouldDownload) {
        Write-Host "Downloading the package"
        $wc = new-object System.Net.WebClient 
        $downloadUrl = "$octopusServer/api/packages/packages-$fullPackageName/raw?apiKey=$octopusApiKey"
        $wc.DownloadFile($downloadUrl, $fullFilePath)
    }

    return $fullFilePath
}