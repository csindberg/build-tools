function GetRepositoryRootFolder([string] $folder = $PSScriptRoot)
{
    $subFolders = Get-ChildItem -Path $folder -Name

    $parent = Split-Path -parent $folder

    if($parent -eq "") {
        return ""
    }

    if($subFolders -notcontains "Tools") {
        return GetRepositoryRootFolder $parent
    }

    if($subFolders -notcontains "Src") {
        return GetRepositoryRootFolder $parent
    }

    return $folder
}

function GetSrcFolder()
{
    $rootFolder = GetRepositoryRootFolder
    $srcFolder = $rootFolder + "\Src"
    
    return $srcFolder
}

function GetSolutionName()
{
    $srcFolder = GetSrcFolder
    $slnFileItem = Get-ChildItem ($srcFolder+"\*.sln") | Get-Item
    $slnFile = (Split-Path -Path $slnFileItem -Leaf).Split(".")[0];
    
    $slnFile
}