﻿function DownloadAndInstallStecoreFolder($sitecoreVersion, $installFolder, $octopusServer, $octopusApiKey) {
    $deployFolder = $installFolder

    $tmpDir = $env:TEMP
    $packageName = "Sitecorefolder-$sitecoreVersion"

    $tmpUnpackDir = [System.IO.Path]::Combine($tmpDir, "$packageName")
    $tmpFile = [System.IO.Path]::Combine($tmpDir, "$packageName.nupkg")
    $tmpZip = [System.IO.Path]::Combine($tmpDir, "$packageName.zip")

    
    Write-Host "Checking for file '$tmpFile'"

    $shouldDownload = $true

    if(Test-Path -PathType Leaf $tmpFile) {
        Write-Host "File found. Checking hash."
        # Use url below to get the sha1 hash use to see if we already have the current file
        $hashRequestUrl = "$octopusServer/api/packages/packages-" + $packageName + "?apiKey=$octopusApiKey"
        $hash = Invoke-WebRequest $hashRequestUrl |
        ConvertFrom-Json |
        Select -ExpandProperty Hash
        $calculatedHash = Get-FileHash -Algorithm SHA1 $tmpFile | select -ExpandProperty Hash
        if($calculatedHash -ne $hash) {
            Write-Host "Hash mismatch. Redownloading package."
            $shouldDownload = $true
        } else {
            Write-Host "Hash matches. Package integrity verified."
            $shouldDownload = $false
        }
    }

    if($shouldDownload) {
        Write-Host "Downloading the package"
        $wc = new-object System.Net.WebClient 
        $wc.DownloadFile("$octopusServer/api/packages/packages-$packageName/raw?apiKey=$octopusApiKey", $tmpFile)
    }

    Rename-Item -Path $tmpFile -NewName $tmpZip
    Write-Host "Unpacking... This might take a while grab a cup of coffee!"
    Expand-Archive $tmpZip -DestinationPath $tmpUnpackDir -Force
    Rename-Item -Path $tmpZip -NewName $tmpFile
    Write-Host "Copying the folder from '$tmpUnpackDir to '$deployFolder'"
    Copy-Item -Recurse -Path "$tmpUnpackDir\sitecore" -Destination "$deployFolder" -Container -Force
    Write-Host "Done!"
}