﻿Add-Type -LiteralPath "$PSScriptRoot\Microsoft.Web.XmlTransform.dll"

function XmlDocTransform($xml, $xdt)
{
    if (!$xml -or !(Test-Path -path $xml -PathType Leaf)) {
        throw "Base file '$xml' not found.";
    }
    if (!$xdt -or !(Test-Path -path $xdt -PathType Leaf)) {
        throw "Transform file '$xdt' not found.";
    }
    
    $xmldoc = New-Object Microsoft.Web.XmlTransform.XmlTransformableDocument;
    $xmldoc.PreserveWhitespace = $true
    $xmldoc.Load($xml);

    $transf = New-Object Microsoft.Web.XmlTransform.XmlTransformation($xdt);
    if ($transf.Apply($xmldoc) -eq $false)
    {
        throw "Transformation failed when transforming '$xml' with '$xdt'."
    }
    $xmldoc.Save($xml);
}