﻿param([Parameter(Mandatory=$true)][String]$XmlPath, [Parameter(Mandatory=$true)][String[]] $ConfigNames)
Remove-Module [C]onfigurationTransforms
Import-Module "$PSScriptRoot\dependencies\ConfigurationTransforms.psm1"

if(!$XmlPath) {
    throw "You need to supply a xmlPath parameter";
}

Write-Host "Transforming configs under '$XmlPath'" -ForegroundColor Green

if(!(Test-Path -Path $XmlPath -PathType Container)) {
    throw "Path '$XmlPath' not found";
}

foreach($configName in $ConfigNames) {
    Write-Host "Locating files for config '$configName'"
    $transformFiles = Get-ChildItem -Path $XmlPath -Filter *.$configName.config -Recurse | Select FullName
    foreach($file in $transformFiles) {
        $baseFile = $file.FullName.ToLower().Replace("$configName.config".ToLower(), "config")
        $transformFile = $file.FullName
    
        if(Test-Path $baseFile -PathType Leaf) {
            Write-Host transforming $baseFile with $transformFile
            
            Try {
                XmlDocTransform $baseFile $transformFile
            } Catch {
                Write-Host Error happened: $_.Exception.Message -ForegroundColor Red
            }
        } else {
            Write-Host "No base file '$baseFile' found for '$transformFile'"
        }
    }
}