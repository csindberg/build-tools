﻿param([Parameter(Mandatory=$true)][String]$PathToClean)

Write-Host "Cleaning transforms under '$PathToClean'" -ForegroundColor Green

$result = Get-ChildItem -Recurse -Path $PathToClean -File -Filter "*.config"

foreach($file in $result) {
    $match = Select-String -Path $file -Pattern "http://schemas.microsoft.com/XML-Document-Transform"
    if(!$match) {
        continue;
    }

    Write-Host Deleting transform file $match.Path
    Remove-Item -LiteralPath $match.Path
}