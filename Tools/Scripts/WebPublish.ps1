﻿param([String]$RelativeDeployDir="wwwroot", [String]$AbsoluteDeployDir, [ValidateSet("2.0", "3.5", "4.0", "12.0", "14.0", "15.0")][String]$dotNetVersion = "15.0", [string[]]$Configurations = @("all", "Debug", "Local") )

Remove-Module [S]olutionFunctions
Import-Module "$PSScriptRoot\dependencies\SolutionFunctions.psm1"
Remove-Module [m]sbuild
Import-Module "$PSScriptRoot\dependencies\msbuild.psm1"

$rootFolder = GetRepositoryRootFolder $PSScriptRoot
$projectFile = [System.IO.Path]::Combine($rootFolder,  "src\Impact.Website\Impact.Website.csproj")

if(!$AbsoluteDeployDir) {
    $DeployDir = [System.IO.Path]::Combine($rootFolder, $RelativeDeployDir)
} else {
    $DeployDir = $AbsoluteDeployDir
}

$websiteFolder = [System.IO.Path]::Combine($deployDir, "Website")
$msbuildExe = Get-MsBuildExec $dotNetVersion

$LASTEXITCODE = 0
&$msbuildExe $projectFile /T:WebPublish /P:DeployDir=$websiteFolder /P:VisualStudioVersion=$dotNetVersion
if($LASTEXITCODE -ne 0) {
    Write-Host msbuild encountered an error.
    exit $LASTEXITCODE
}

$transformScript = [System.IO.Path]::Combine($PSScriptRoot, "TransformConfigs.ps1")
$featureTransforms = [System.IO.Path]::Combine($PSScriptRoot, "FeatureTransforms.ps1")
$cleanupScript = [System.IO.Path]::Combine($PSScriptRoot, "CleanTransforms.ps1")

&$transformScript -XmlPath $websiteFolder -ConfigNames $Configurations
&$featureTransforms -XmlPath $websiteFolder -FeaturePath "App_Config\Impact" -ConfigNames $Configurations
&$cleanupScript -PathToClean $websiteFolder

Write-Host Publish complete -ForegroundColor Green