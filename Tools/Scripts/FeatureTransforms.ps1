param([Parameter(Mandatory=$true)][String]$XmlPath, [Parameter(Mandatory=$true)][String]$FeaturePath, [Parameter(Mandatory=$true)][String[]] $ConfigNames)
Remove-Module [C]onfigurationTransforms
Import-Module "$PSScriptRoot\dependencies\ConfigurationTransforms.psm1"

$fPath = [System.IO.Path]::Combine($XmlPath, $FeaturePath)
Write-Host "Locating files for Feature transforms in $fPath"

foreach($config in $ConfigNames) {
    $featureFiles = Get-ChildItem -Path $fPath -Filter *.$config.config -Recurse | Select FullName

    $baseFile = "$XmlPath\Web.config"
    foreach($file in $featureFiles) {
        $transformFile = $file.FullName
    
        Write-Host transforming $baseFile with $transformFile

        Try {
            XmlDocTransform $baseFile $transformFile
        } Catch {
            Write-Host Error happened: $_.Exception.Message -ForegroundColor Red
        }
    }
}