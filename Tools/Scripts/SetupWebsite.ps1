param (
    [string]$mode = $( Read-Host "Input mode (install or delete)" ),
    [string]$apiKey
 )

Remove-Module [W]ebAdministration
Import-Module "WebAdministration"
Remove-Module [H]osts
Import-Module "$PSScriptRoot\dependencies\Hosts.psm1"
Remove-Module [S]olutionFunctions
Import-Module "$PSScriptRoot\dependencies\SolutionFunctions.psm1"
Remove-Module [S]itecorePackaging
Import-Module "$PSScriptRoot\dependencies\SitecorePackaging.psm1"

if(-not [string]::IsNullOrEmpty($apiKey)) {
    Write-Host "Using the apiKey provided as parameter"
}

if([string]::IsNullOrEmpty($apiKey)) {
    Write-Host "Using apiKey from registry"
    $apiKey = Get-ItemProperty "hklm:\software\Impact" | Select -ExpandProperty OctopusApi
}

if([string]::IsNullOrEmpty($apiKey)) {
    Write-Host "Using apiKey from Environment"
    $apiKey = $env:OCTOPUS_API
}

if([string]::IsNullOrWhiteSpace($apiKey)) {
    Write-Host "No Api key set. You need to supply it in one of the following ways:"
    Write-Host "\tRegistry key in : hklm:\software\Impact\OctopusApi"
    Write-Host "\tEnvironment variable 'OCTOPUS_API'"
    Write-Host "\tParameter for this script"
    exit 1
}

$sitecoreVersion = "8.2.161221"
$octopusServer = "https://cd.impact.dk"
$hostFile = "C:\Windows\System32\drivers\etc\hosts"
$srcFolder = GetSrcFolder
$rootFolder = GetRepositoryRootFolder
$solutionName = GetSolutionName
$deployFolder = $rootFolder + "\wwwroot"
$iisAppPool = "IIS:\AppPools\" +$solutionName
$iisSite = "IIS:\Sites\" + $solutionName
$iisWebsitePath = $deployFolder + "\Website"
$dataPath = $deployFolder + "\Data"
$iisHostName = $solutionName + ".localhost"

Write-Host "Debug information:"
Write-Host "   rootFolder             :" $rootFolder
Write-Host "   srcFolder              :" $srcFolder
Write-Host "   solutionName           :" $solutionName
Write-Host "   deployFolder           :" $deployFolder
Write-Host "   iisAppPool             :" $iisAppPool
Write-Host "   iisSite                :" $iisSite
Write-Host "   sitecoreSourceWithPath :" $sitecoreSourceWithPath
Write-Host "   deployFolderWithZip    :" $deployFolderWithZip
Write-Host ""
Write-Host ""

If($mode.Equals("install"))
{
    If(Test-Path $deployFolder) {
        Write-Host "Deleting the deployFolder: '$deployFolder'"
        Remove-Item -Path $deployFolder -Force -Recurse
    }

    #Create website deploy folder
    New-Item -ItemType Directory -Force -Path $deployFolder
    Write-Host -BackgroundColor Green "Deploy folder created"
    
    #Create app pool
    If(!(test-path $iisAppPool))
    {
        $pool = New-Item $iisAppPool
        $pool.processModel.identityType = "NetworkService"
        $pool.managedRuntimeVersion = 'v4.0'
        $pool | set-item
        Write-Host -BackgroundColor Green "IIS AppPool created"
    }
    else
    {
        Write-Host "IIS AppPool already exists"
    }
    
    #Create IIS site and point it to website folder
    If(!(test-path $iisSite))
    {
        New-Item $iisSite -bindings @{protocol="http";bindingInformation=":80:" + $iisHostName} -physicalPath $iisWebsitePath
        Write-Host -BackgroundColor Green "IIS Website created"
    
        Set-ItemProperty $iisSite -name applicationPool -value $solutionName
        Write-Host -BackgroundColor Green "AppPool connected to Website"

        Add-Host $hostFile "127.0.0.0.1" $iisHostName
        Write-Host -BackgroundColor Green $iisHostName " added to host file " $hostFile
    }
    
    Set-ItemProperty $iisSite -name "physicalPath" -Value $iisWebsitePath
    
    $tmpFolder = [System.IO.Path]::Combine($rootFolder, "tmp")

    if(!(Test-Path -Path $tmpFolder)) {
        New-Item -Path $tmpFolder -ItemType Directory | Out-Null
    }

    #Initialize the sitecore folder
    $sitecoreFolder = ScPack-VerifyPackage $tmpFolder "SitecoreFolder" $sitecoreVersion $octopusServer $apiKey
    Write-Host "Extracting the sitecore folder"
    Expand-Archive $sitecoreFolder -DestinationPath $iisWebsitePath -Force
    #Initialize the sitecore data folder
    $sitecoreDataFolder = ScPack-VerifyPackage $tmpFolder "SitecoreDataFolder" $sitecoreVersion $octopusServer $apiKey
    Write-Host "Extracting the data folder"
    Expand-Archive $sitecoreDataFolder -DestinationPath $deployFolder -Force

    $inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [system.security.accesscontrol.PropagationFlags]"None"

    #Add access rights for NetworkService to website folder
    $AclWebsite = Get-Acl $iisWebsitePath
    $ArWebsite =  New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "FullControl", $inherit, $propagation, "Allow")
    $AclWebsite.SetAccessRule($ArWebsite)
    Set-Acl $iisWebsitePath $AclWebsite
    Write-Host -BackgroundColor Green "Network Service has access to Website folder"

    #Add access rights for NetworkService to data folder
    $AclData = Get-Acl $dataPath
    $ArData =  New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "FullControl", $inherit, $propagation, "Allow")
    $AclData.SetAccessRule($ArData)
    Set-Acl $dataPath $AclData
    Write-Host -BackgroundColor Green "Network Service has access to Data folder"
}
elseif($mode.Equals("delete"))
{
    Remove-Item $iisSite -recurse -force -confirm:$false
    Write-Host -BackgroundColor Green "Website removed"

    Remove-Item $iisAppPool -recurse -force -confirm:$false
    Write-Host -BackgroundColor Green "AppPool removed"

    Remove-Host $hostFile "127.0.0.0.1" $iisHostName
    Write-Host -BackgroundColor Green $iisHostName " removed from host file " $hostFile
}
else
{
    Write-Host -BackgroundColor Red "You have to specify a valid mode; either install or delete"
}